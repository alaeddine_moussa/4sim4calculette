package org.esprit.calculette.services;

import javax.ejb.Remote;

@Remote
/**
 * 
 * @author Aladin
 *
 */
public interface CalculetteRemote {
	/**
	 * Cette méthode permet d'additionner deux entiers
	 * @param a int
	 * @param b	int
	 * @return int addition
	 */
	public int add (int a, int b);
	
	/**
	 * Cette méthode peremet de soustraire deux entiers
	 * 
	 * @param a
	 *            int
	 * @param b
	 *            int
	 * @return int addition
	 */
	public int subtract(int a, int b);
	
}
