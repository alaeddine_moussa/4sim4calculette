package org.esprit.calculette.services;

import javax.ejb.Stateless;

/**
 * Session Bean implementation class Calculette
 */
@Stateless
public class Calculette implements CalculetteRemote, CalculetteLocal {

    /**
     * Default constructor. 
     */
    public Calculette() {
    }

	@Override
	/**
	 *
	 */
	public int add(int a, int b) {
		return a+b;
	}

	@Override
	public int subtract(int a, int b) {
		return a-b;
	}

}
